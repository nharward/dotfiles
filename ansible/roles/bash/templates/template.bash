#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

# Set magic variables about the script itself
declare __exe_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
declare __exe_path="${__exe_dir}/$(basename "${BASH_SOURCE[0]}")"
declare __exe_name="$(basename "${__exe_path}")"

# Other defaults
declare __verbosity='none'

# For when you want the user to see some output regardless of verbosity level
_msg() {
    echo "$@"
}

# User debugging, for when you want the user to see output only when -v or -x are used
# This no-op function will be redefined later if -v or -x are used
_info() {
    :
}

# User tracing, for when you want the user to see output only when -x is used
# This no-op function will be redefined later if -x is used
_trace() {
    :
}

_error() {
    echo "$@" >&2
}

_usage() {
    cat << __USAGE__

Usage: sorry, the author has not bothered to update how to run ${0}

Options:
    -h | --help                 show this help
    -v | --verbose              enable verbose output to stderr
    -x | --verbose=trace        enable even more verbose output to stderr and enable bash tracing
__USAGE__
}

set +o nounset # OPTARG can be empty while parsing options, temporarily disable nounset
while getopts ':-:hvx' option
do
    if [[ "${__verbosity}" = "trace" ]]; then
        echo "Processing option '${option}' with argument '${OPTARG}'"
    fi
    case "${option}${OPTARG}" in
        v|-verbose)         __verbosity='verbose';;
        x|-verbose=trace)   __verbosity='trace';;
        h|-help)            _usage; exit 2;;
        -*)                 _error "Unknown option -${option}${OPTARG}"; _usage; exit 2;;
        ?*)                 _error "Unknown option -${OPTARG}";          _usage; exit 2;;
    esac
done
shift $((${OPTIND} - 1))
set -o nounset

# Make _info do something if we are verbose
if [[ "${__verbosity}" = 'verbose' ]]
then
    _info() {
        echo "$@" >&2
    }
elif [[ "${__verbosity}" = 'trace' ]]
then
    _info() {
        echo "[${__exe_name} $(date -Iseconds) INFO]" "$@" >&2
    }

    _trace() {
        echo "[${__exe_name} $(date -Iseconds) TRACE]" "$@" >&2
    }

    _error() {
        echo "[${__exe_name} $(date -Iseconds) ERROR]" "$@" >&2
    }
fi

# Turn on bash tracing if the '--verbose=trace' or '-x' options were used
[[ "${__verbosity}" = 'trace' ]] && set -o xtrace

_main() {
    _info 'I am only a template and do nothing.'
    exit 1
}

_main "$@"
