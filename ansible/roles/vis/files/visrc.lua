-- load standard vis module, providing parts of the Lua API
require('vis')
require('plugins/complete-word')
require('plugins/filetype')
require('plugins/vis-tables')

vis.events.subscribe(vis.events.INIT, function()
    -- Global configuration options
    vis:command('set theme base16-dracula')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
    vis:command('set expandtab off')
    vis:command('set numbers on')
    vis:command('set relativenumbers on')
    vis:command('set showeof on')
    vis:command('set shownewlines on')
    vis:command('set showspaces on')
    vis:command('set showtabs on')
    vis:command('set tablemode on')
    vis:command('set tabwidth 4')
end)
