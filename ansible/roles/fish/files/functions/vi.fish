function vi
	if test (uname) = Linux
		set VIPROGS vis nvim vim vi
	else if test (uname) = Darwin
		# vis is vise on MacOSX
		set VIPROGS vise nvim vim vi
	end

	for viprog in $VIPROGS
		if command -q $viprog
			if test $viprog = nvim -a {$HOME}/.config/nvim/init.vim -nt {$HOME}/.local/share/nvim/plugged
				# Update Neovim plugins
				command $viprog +PlugInstall +PlugUpdate +q +q
				touch {$HOME}/.local/share/nvim/plugged
			end
			$viprog $argv
				and break
				or exit $status
		end
	end
end
