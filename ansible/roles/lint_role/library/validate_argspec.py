#!/usr/bin/python

#from __future__ import (absolute_import, division, print_function)
import os
import yaml
__metaclass__ = type

DOCUMENTATION = r'''
---
module: validate_argspec.yml

short_description: Validates role default arguments match the arg spec.

options:
    role_path:
        description: The path to the role
        required: true
        type: path

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Verify a role
  validate_argspec:
    role_path: /etc/ansible/roles/some_role_dir
'''

RETURN = r'''
# Nothing is returned on success
    "role_arg_spec": {
        "argument_specs": {
            "main": {
                "options": {
                    "bash_install": {
                        "default": true,
                        "description": "Whether to add (true) or remove (false)",
                        "type": "bool"
                    }
                },
                "short_description": "Package and $HOME configuration"
            }
        }
    },
    "role_defaults": {
        "bash_install": true,
        "bash_package": "bash"
    }
'''

from ansible.module_utils.basic import AnsibleModule


def has_default(arg_spec_pair):
    arg_name, arg_spec = arg_spec_pair
    return 'default' in arg_spec

# Returns a dict() of arg spec options, or None if there is an issue
def fetch_args_with_defaults(arg_spec):
    try:
        arg_dict = arg_spec['argument_specs']['main']['options']
        return dict(filter(has_default, arg_dict.items()))
    except KeyError as ke:
        return None


def run_module():
    module_arg_spec = dict(
        role_path=dict(type='path', required=True),
    )

    result = dict(changed=False)
    module = AnsibleModule(
        argument_spec=module_arg_spec,
        supports_check_mode=False
    )

    role_path = module.params['role_path']
    role_arg_spec = None
    role_defaults = None

    arg_spec_path = os.path.join(role_path, 'meta/argument_specs.yml')
    try:
        with open(arg_spec_path, "r") as arg_spec_stream:
            role_arg_spec = yaml.safe_load(arg_spec_stream)
            result['role_arg_spec']=role_arg_spec
    except IOError as e:
        module.fail_json(msg=f"Error reading role arg spec '{arg_spec_path}': {e}", **result)
        sys.exit(1)
    except yaml.YAMLError as ye:
        module.fail_json(msg=f"Error parsing role arg spec '{arg_spec_path}': {ye}", **result)
        sys.exit(1)

    defaults_path = os.path.join(role_path, 'defaults/main.yml')
    try:
        with open(defaults_path, "r") as defaults_stream:
            role_defaults = yaml.safe_load(defaults_stream)
            result['role_defaults']=role_defaults
    except IOError as e:
        module.fail_json(msg=f"Error reading role defaults '{defaults_path}': {e}", **result)
        sys.exit(1)
    except yaml.YAMLError as ye:
        module.fail_json(msg=f"Error parsing role defaults '{defaults_path}': {ye}", **result)
        sys.exit(1)

    args_with_defaults = fetch_args_with_defaults(role_arg_spec)
    for arg_name, arg in args_with_defaults.items():
        expected = arg['default']
        actual = role_defaults[arg_name]
        if actual != expected:
            module.fail_json(msg=f"Role argument '{arg_name}' has default of '{actual}', expected '{expected}'", **result)
            sys.exit(1)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
