# Nat's dotfiles

## Synopsis

Applies my preferred environment to a server or workstation. It's meant to run
on local hosts only and doesn't have a proper inventory.

## Requirements

- [Ansible](https://www.ansible.com/) >= 2.4.0.0
- [Arch](https://archlinux.org/) | [Debian](https://debian.org/), or MacOS + [Homebrew](https://brew.sh/) (base install only)

## Usage

There are two playbooks: one for a base system installation (`base.yml`) and another
for user-level Linux desktops/laptops (`desktop.yml`).

### One-time

```shell
$ git clone https://gitlab.com/nharward/dotfiles.git
```

### Ongoing

```shell
$ cd <path to dotfiles>/ansible
$ git pull
$ ansible-playbook base.yml # and/or desktop.yml
```
